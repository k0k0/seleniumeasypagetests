package test;

import org.testng.Assert;
import org.testng.annotations.Test;
import pageObjects.LandingPage;

public class SimpleFormTests extends TestBase {

    @Test
    public void simpleInputTest() {

        LandingPage landingPage = new LandingPage();
        String message = landingPage
                .noThanksButtonClick()
                .basicButtonClick()
                .simpleFormDemoButton()
                .sendMessage("Test message 1")
                .showMessageButtonClick()
                .MessageDisplayedCheck();

        Assert.assertEquals(message, "Test message 1");
    }

    @Test
    public void twoInputFieldsTest() {

        LandingPage landingPage = new LandingPage();
        String sumValue =landingPage
                .noThanksButtonClick()
                .basicButtonClick()
                .simpleFormDemoButton()
                .setNumberFieldA(5)
                .setNumberFieldB(10)
                .getTotalButtonClick()
                .sumCheck();

        Assert.assertEquals(sumValue, "15");
    }

}
