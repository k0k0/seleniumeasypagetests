package test;

import org.testng.Assert;
import org.testng.annotations.Test;
import pageObjects.BasicRadioButtonPage;
import pageObjects.LandingPage;

public class BasicRadioButtonTests extends TestBase {

    @Test
    public void setRadioButtonToMaleTest() {

        LandingPage landingPage = new LandingPage();

        String radioButtonText = landingPage
                .noThanksButtonClick()
                .basicButtonClick()
                .basicRadioButton()
                .setMaleRadioButton()
                .checkGenderButtonClick()
                .radioButtonTextCheck();

        Assert.assertEquals(radioButtonText, "Radio button 'Male' is checked");
    }

    @Test
    public void setRadioButtonToFemaleTest() {

        LandingPage landingPage = new LandingPage();

        String radioButtonText = landingPage
                .noThanksButtonClick()
                .basicButtonClick()
                .basicRadioButton()
                .setFemaleRadioButton()
                .checkGenderButtonClick()
                .radioButtonTextCheck();

        Assert.assertEquals(radioButtonText, "Radio button 'Female' is checked");
    }

    @Test
    public void maleInAllAgesGroupTest() {
        LandingPage landingPage = new LandingPage();

        String youngestGroupRadioButtonText = landingPage
                .noThanksButtonClick()
                .basicButtonClick()
                .basicRadioButton()
                .setMaleGroupRadio()
                .setYoungestGroup()
                .groupValuesButtonClick()
                .groupRadioButtonTextCheck();

        Assert.assertEquals(youngestGroupRadioButtonText, "Sex : Male\n" +
                "Age group: 0 - 5");

        BasicRadioButtonPage basicRadioButtonPage = new BasicRadioButtonPage();

        String middleGroupRadioButtonCheck = basicRadioButtonPage
                .setMiddleGroup()
                .groupValuesButtonClick()
                .groupRadioButtonTextCheck();

        Assert.assertEquals(middleGroupRadioButtonCheck, "Sex : Male\n" +
                "Age group: 5 - 15");

        String seniorGroupRadioButtonCheck = basicRadioButtonPage
                .setSeniorGroup()
                .groupValuesButtonClick()
                .groupRadioButtonTextCheck();

        Assert.assertEquals(seniorGroupRadioButtonCheck, "Sex : Male\n" +
                "Age group: 15 - 50");

    }
}
