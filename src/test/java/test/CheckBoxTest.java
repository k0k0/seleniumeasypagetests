package test;

import org.testng.Assert;
import org.testng.annotations.Test;
import pageObjects.LandingPage;

public class CheckBoxTest extends TestBase {

    @Test
    public void singleCheckboxTest() {

        LandingPage landingPage = new LandingPage();

        String checkBoxMessage = landingPage
                .noThanksButtonClick()
                .basicButtonClick()
                .checkBoxDemoButton()
                .singleCheckboxClick()
                .singleCheckboxClickedGetText();

        Assert.assertEquals(checkBoxMessage, "Success - Check box is checked");
    }

    @Test
    public void checkAllCheckboxIsNotSelected() {

        LandingPage landingPage = new LandingPage();

        int numberOfActiveCheckboxes = landingPage
                .noThanksButtonClick()
                .basicButtonClick()
                .checkBoxDemoButton()
                .checkboxIsSelectedCheck();

        Assert.assertEquals(numberOfActiveCheckboxes, 0);
    }

    @Test
    public void checkAllCheckboxIsSelected() {
        LandingPage landingPage = new LandingPage();

        int numberOfActiveCheckboxes = landingPage
                .noThanksButtonClick()
                .basicButtonClick()
                .checkBoxDemoButton()
                .checkUncheckAllButtonClick()
                .checkboxIsSelectedCheck();

        Assert.assertEquals(numberOfActiveCheckboxes, 4);
    }

}
