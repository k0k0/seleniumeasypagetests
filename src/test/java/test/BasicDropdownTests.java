package test;

import org.testng.Assert;
import org.testng.annotations.Test;
import pageObjects.LandingPage;

public class BasicDropdownTests extends TestBase{

    @Test
    public void setMondayFromSingleDropdownList() {

        LandingPage landingPage = new LandingPage();

        String day = landingPage
                .noThanksButtonClick()
                .basicButtonClick()
                .basicDropdownList()
                .setMondayFromSingleSelectDropdown()
                .checkDay();

        Assert.assertEquals(day, "Day selected :- Monday");
    }

    @Test
    public void setFridayFromSingleDropdownList() {

        LandingPage landingPage = new LandingPage();

        String day = landingPage
                .noThanksButtonClick()
                .basicButtonClick()
                .basicDropdownList()
                .setFridayFromSingleSelectDropdown()
                .checkDay();

        Assert.assertEquals(day, "Day selected :- Friday");
    }
}
