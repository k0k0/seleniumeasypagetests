package pageObjects;

import driver.manager.DriverManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import waits.WaitForElement;

public class BasicDropdownListPage {

    Logger logger = LogManager.getRootLogger();

    @FindBy(id = "select-demo")
    private WebElement singleSelect;

    @FindBy(className = "selected-value")
    private WebElement checkDayText;

    public BasicDropdownListPage() {
        PageFactory.initElements(DriverManager.getWebDriver(), this);
    }

    public BasicDropdownListPage setMondayFromSingleSelectDropdown() {
        Select dropdownMenu = new Select(singleSelect);
        dropdownMenu.selectByVisibleText("Monday");
        logger.info("Set monday on dropdown list.");
        return this;
    }

    public BasicDropdownListPage setFridayFromSingleSelectDropdown() {
        Select dropdownMenu = new Select(singleSelect);
        dropdownMenu.selectByIndex(6);
        logger.info("Friday is set in the dropdown list.");
        return this;
    }

    public String checkDay() {
        WaitForElement.waitUntilElementsVisible(checkDayText);
        logger.info("The day has been checked");
        return checkDayText.getText();
    }

}
