package pageObjects;

import driver.manager.DriverManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import waits.WaitForElement;

public class LandingPage {

    private Logger logger = LogManager.getRootLogger();

    @FindBy(xpath = "//a[text()='No, thanks!']")
    private WebElement noThanksBtn;

    @FindBy(id = "basic_example")
    private WebElement basicTestBtn;

    public LandingPage() {
        PageFactory.initElements(DriverManager.getWebDriver(), this);
    }

    public LandingPage noThanksButtonClick() {
        WaitForElement.waitUntilElementIsClickable(noThanksBtn);
        noThanksBtn.click();
        logger.info("Clicked on pop-up window.");
        return this;
    }

    public BasicExamplesPage basicButtonClick() {
        WaitForElement.waitUntilElementIsClickable(basicTestBtn);
        basicTestBtn.click();
        logger.info("Clicked on basic tests button.");
        return new BasicExamplesPage();
    }
}
