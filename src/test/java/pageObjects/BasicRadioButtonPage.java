package pageObjects;

import driver.manager.DriverManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import waits.WaitForElement;

public class BasicRadioButtonPage {

    Logger logger = LogManager.getRootLogger();

    @FindBy(xpath = "//input[@name='optradio' and @value='Male']")
    private WebElement maleRadio;

    @FindBy(xpath = "//input[@name='optradio' and @value='Female']")
    private WebElement femaleRadio;

    @FindBy(id = "buttoncheck")
    private WebElement checkedValueBtn;

    @FindBy(className = "radiobutton")
    private WebElement radioButtonTextCheck;

    @FindBy(xpath = "//input[@value='Male' and @name='gender']")
    private WebElement maleGroupRadio;

    @FindBy(xpath = "//input[@value='Female' and @name='gender']")
    private WebElement femaleGroupRadio;

    @FindBy(xpath = "//input[@value='0 - 5']")
    private WebElement youngestGroupAge;

    @FindBy(xpath = "//input[@value='5 - 15']")
    private WebElement middleGroupAge;

    @FindBy(xpath = "//input[@value='15 - 50']")
    private WebElement seniorsGroupAge;

    @FindBy(xpath = "//button[text()='Get values']")
    private WebElement groupValuesBtn;

    @FindBy(className = "groupradiobutton")
    private WebElement groupRadioButtonTextCheck;

    public BasicRadioButtonPage() {
        PageFactory.initElements(DriverManager.getWebDriver(), this);
    }

    public BasicRadioButtonPage setMaleRadioButton() {
        WaitForElement.waitUntilElementIsClickable(maleRadio);
        maleRadio.click();
        logger.info("The male radio button is on.");
        return this;
    }

    public BasicRadioButtonPage setFemaleRadioButton() {
        WaitForElement.waitUntilElementIsClickable(femaleRadio);
        femaleRadio.click();
        logger.info("The female radio button is on.");
        return this;
    }

    public BasicRadioButtonPage checkGenderButtonClick() {
        checkedValueBtn.click();
        logger.info("The gender check button where clicked.");
        return this;
    }

    public String radioButtonTextCheck() {
        WaitForElement.waitUntilElementsVisible(radioButtonTextCheck);
        logger.info("The text has been checked.");
        return radioButtonTextCheck.getText();
    }

    public BasicRadioButtonPage setMaleGroupRadio() {
        WaitForElement.waitUntilElementIsClickable(maleGroupRadio);
        maleGroupRadio.click();
        logger.info("The male group radio button is on.");
        return this;
    }

    public BasicRadioButtonPage setFemaleGroupRadio() {
        WaitForElement.waitUntilElementIsClickable(femaleGroupRadio);
        femaleGroupRadio.click();
        logger.info("The female group radio button is on.");
        return this;
    }

    public BasicRadioButtonPage setYoungestGroup() {
        youngestGroupAge.click();
        logger.info("The youngest group has been set.");
        return this;
    }

    public BasicRadioButtonPage setMiddleGroup() {
        middleGroupAge.click();
        logger.info("The middle group has been set");
        return this;
    }

    public BasicRadioButtonPage setSeniorGroup() {
        seniorsGroupAge.click();
        logger.info("The seniors group has been set");
        return this;
    }

    public BasicRadioButtonPage groupValuesButtonClick() {
        groupValuesBtn.click();
        logger.info("The group values button where clicked.");
        return this;
    }

    public String groupRadioButtonTextCheck() {
        WaitForElement.waitUntilElementsVisible(groupRadioButtonTextCheck);
        logger.info("The group text has been checked.");
        return groupRadioButtonTextCheck.getText();
    }

}
