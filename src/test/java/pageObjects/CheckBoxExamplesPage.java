package pageObjects;

import driver.manager.DriverManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import waits.WaitForElement;

import java.util.List;

public class CheckBoxExamplesPage {

    Logger logger = LogManager.getRootLogger();

    @FindBy(id = "isAgeSelected")
    private WebElement singleCheckbox;

    @FindBy(id = "txtAge")
    private WebElement singleCheckboxSuccessText;

    @FindBys(@FindBy(className = "cb1-element"))
    private List<WebElement> checkboxList;

    @FindBy(id = "check1")
    private WebElement checkAllBtn;

    public CheckBoxExamplesPage() {
        PageFactory.initElements(DriverManager.getWebDriver(), this);
    }

    public CheckBoxExamplesPage singleCheckboxClick() {
        WaitForElement.waitUntilElementIsClickable(singleCheckbox);
        singleCheckbox.click();
        logger.info("Single checkbox has been clicked.");
        return this;
    }

    public String singleCheckboxClickedGetText() {
        WaitForElement.waitUntilElementsVisible(singleCheckboxSuccessText);
        logger.info("The text after clicking on a single checkbox has been checked" );
        return singleCheckboxSuccessText.getText();
    }

    public int checkboxIsSelectedCheck() {
        int numberOfSelected = 0;
        for (WebElement i : checkboxList) {
            if (i.isSelected()) {
                numberOfSelected++;
            }
        }
        return numberOfSelected;
    }

    public CheckBoxExamplesPage checkUncheckAllButtonClick() {
        checkAllBtn.click();
        logger.info("Check all / uncheck all button has been clicked");
        return this;
    }





}
