package pageObjects;

import driver.manager.DriverManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import waits.WaitForElement;

public class SingleFormDemoPage {

    Logger logger = LogManager.getRootLogger();

    @FindBy(id = "user-message")
    private WebElement userMessageField;

    @FindBy(xpath = "//button[text()='Show Message']")
    private WebElement showMessageBtn;

    @FindBy(id = "display")
    private WebElement messageDisplayed;

    @FindBy(id = "sum1")
    WebElement inputNumberFieldA;

    @FindBy(id = "sum2")
    WebElement inputNumberFieldB;

    @FindBy(xpath = "//button[text()='Get Total']")
    WebElement getTotalBtn;

    @FindBy(id = "displayvalue")
    WebElement sumValue;

    public SingleFormDemoPage() {
        PageFactory.initElements(DriverManager.getWebDriver(), this);
    }

    public SingleFormDemoPage sendMessage(String message) {
        WaitForElement.waitUntilElementsVisible(userMessageField);
        userMessageField.clear();
        userMessageField.sendKeys(message);
        logger.info("The message has been typed.");
        return this;
    }

    public SingleFormDemoPage showMessageButtonClick() {
        showMessageBtn.click();
        logger.info("Show message button has been clicked.");
        return this;
    }

    public String MessageDisplayedCheck() {
        WaitForElement.waitUntilElementsVisible(messageDisplayed);
        logger.info("The text after clicked the show message button has been checked.");
        return messageDisplayed.getText();
    }

    public SingleFormDemoPage setNumberFieldA (int number) {
        inputNumberFieldA.clear();
        inputNumberFieldA.sendKeys(String.valueOf(number));
        logger.info("The number in field A has been typed.");
        return this;
    }

    public SingleFormDemoPage setNumberFieldB (int number) {
        inputNumberFieldB.clear();
        inputNumberFieldB.sendKeys(String.valueOf(number));
        logger.info("The number in field B has been typed.");
        return this;
    }

    public SingleFormDemoPage getTotalButtonClick() {
        getTotalBtn.click();
        logger.info("The get total button has been clicked.");
        return this;
    }

    public String sumCheck() {
        WaitForElement.waitUntilElementsVisible(sumValue);
        logger.info("The sum of numbers A and B has been checked.");
        return sumValue.getText();
    }
}
