package pageObjects;

import driver.manager.DriverManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import waits.WaitForElement;

import java.util.List;

public class BasicExamplesPage {

    Logger logger = LogManager.getRootLogger();

    @FindBys(@FindBy(xpath = "//div[@id='basic']//div[@class='list-group']/a"))
    private List<WebElement> basicList;

    public BasicExamplesPage() {
        PageFactory.initElements(DriverManager.getWebDriver(), this);
    }

    public SingleFormDemoPage simpleFormDemoButton() {
        WaitForElement.waitUntilElementIsClickable(basicList.get(0));
        basicList.get(0).click();
        logger.info("Clicked on single form demo button.");
        return new SingleFormDemoPage();
    }

    public CheckBoxExamplesPage checkBoxDemoButton() {
        WaitForElement.waitUntilElementIsClickable(basicList.get(1));
        basicList.get(1).click();
        logger.info("Clicked on checkbox demo button.");
        return new CheckBoxExamplesPage();
    }

    public BasicRadioButtonPage basicRadioButton() {
        WaitForElement.waitUntilElementIsClickable(basicList.get(2));
        basicList.get(2).click();
        logger.info("Clicked on basic radio button.");
        return new BasicRadioButtonPage();
    }

    public BasicDropdownListPage basicDropdownList() {
        WaitForElement.waitUntilElementIsClickable(basicList.get(3));
        basicList.get(3).click();
        logger.info("Clicked on basic dropdown list button.");
        return new BasicDropdownListPage();
    }
}
